<?php

namespace App\Controller;
use App\Entity\Groups;
use App\Entity\User;
use App\Entity\UserGroup;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GroupController extends AbstractController
{
    /**
     * @Route("/group", name="group")
     */
    public function index()
    {
        return $this->render('group/index.html.twig', [
            'controller_name' => 'GroupController',
        ]);
    }

    /**
     * @Route("/group/insert", name="insert_group")
     */
    public function insert()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $group = new Groups();
        $group->setName($_POST['name']);
        $entityManager->persist($group);
        $entityManager->flush();
        return $this->redirectToRoute('show');
    }

       /**
     * Matches /group/delete/{id}
     * @Route("/group/delete/{id}", name="delete_group" , methods={"DELETE" , "GET"})
     * 
     *
     */
    public function delete(Request $request , $id)
    {

        $group = $this->getDoctrine()->getRepository(Groups::class)->find($id);
        // dd($group);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($group);
        $entityManager->flush();
        $response = new Response();
        $response->send();
    }


    /**
     * Matches /group/show/{id}
     * @Route("/group/show/{id}", name="show_group" , methods={"GET"})
     */
    public function show_group($id)
    {
        $group = $this->getDoctrine()->getRepository(Groups::class)->find($id);
        // dd($group->users);
        // $groups=$this->getDoctrine()->getRepository(Groups::class)->findAll();

        return $this->render('group/show.html.twig', [
            'controller_name' => 'UserController',
            'group' => $group ,
            // 'groups' => $groups
        ]);
    }


     /**
     * Matches /group/user_delete/{user_id}/{group_id}
     * @Route("/group/user_delete/{user_id}/{group_id}", name="delete_user_Group" , methods={"DELETE" , "GET"})
     * 
     *
     */
    public function delete_user_Group(Request $request , $user_id , $group_id)
    {

        $group = $this->getDoctrine()->getRepository(UserGroup::class)
        ->findBy(['user_id'=>$user_id , 'groups_id' => $group_id]);
        
        
       
        foreach($group as $value)
        {
            $user = $this->getDoctrine()->getRepository(UserGroup::class)
        ->find($value->id);
          $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();
        }
        
        return $this->redirectToRoute('show_group',['id'=>$group_id]);
    }
}
