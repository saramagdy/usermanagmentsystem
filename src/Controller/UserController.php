<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Groups;
use App\Entity\UserGroup;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AbstractController
{
    // /**
    //  * Matches /user
    //  * @Route("/", name="user")
    //  */
    // public function index()
    // {

    //     return $this->render('user/index.html.twig', [
    //         'controller_name' => 'UserController',
    //     ]);
    // }

    /**
     * Matches /
     * @Route("/", name="show")
     * 
     */
    public function show()
    {
        $users=$this->getDoctrine()->getRepository(User::class)->findAll();
        $groups=$this->getDoctrine()->getRepository(Groups::class)->findAll();
        return $this->render('user/home.html.twig', [
            'controller_name' => 'UserController',
            'users'=>$users,
            'groups'=>$groups
        ]);
    }


    // /**
    //  * Matches /user/store
    //  * @Route("/user/store", name="store")
    //  *
    //  */
    // public function store()
    // {
    //     return $this->render('user/store.html.twig', [
    //         'controller_name' => 'UserController',
    //     ]);
    // }

    
    // /**
    //  * Matches /user/insert
    //  * @Route("/user/insert", name="insert")
    //  *
    //  */
    // public function insert()
    // {

    //     $entityManager = $this->getDoctrine()->getManager();
    //     $user = new User();
    //     return $this->render('user/home.html.twig', [
    //         'controller_name' => 'UserController',
    //     ]);
    // }

     /**
     * Matches /user/add
     * @Route("/user/add", name="add")
     *
     */
    public function add()
    {

        return $this->render('user/add.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

      /**
     * Matches /user/add_user
     * @Route("/user/add_user", name="add_user")
     *
     */
    public function add_user(Request $request)
    {
        // dd($_POST['password']);

        $entityManager = $this->getDoctrine()->getManager();
        $user = new User();
        $user->setName($_POST['name']);
        $user->setPassword($_POST['password']);
        $entityManager->persist($user);
        $entityManager->flush();
         return $this->redirectToRoute('show');
    }

      /**
     * Matches /user/delete/{id}
     * @Route("/user/delete/{id}", name="delete_user", methods={"DELETE","GET"})
     *
     */
    public function delete($id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        // dd($user);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();
        $response = new Response();
        $response->send();

        
    }

    /**
     * Matches /user/show/{id}
     * @Route("/user/show/{id}", name="show_user" , methods={"GET"})
     */
    public function show_user($id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $groups=$this->getDoctrine()->getRepository(Groups::class)->findAll();

        return $this->render('user/show.html.twig', [
            'controller_name' => 'UserController',
            'user' => $user ,
            'groups' => $groups
        ]);
    }

    /**
     *  Matches /user/assign/{id}
     * @Route("/user/assign/{id}", name="assign" , methods={"POST" , "GET"})
     */
    public function assign(Request $request , $id)
    {
        // dd($_POST['group_id']);
        $entityManager = $this->getDoctrine()->getManager();
        $user = new UserGroup();
        $user->setUserId($id);
        $user->setGroupsId($_POST['group_id']);

        $entityManager->persist($user);
        $entityManager->flush();
         return $this->redirectToRoute('show_user',['id'=>$id]);
        
    }

}
