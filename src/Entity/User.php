<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="text" , length=100)
     */
    private $name;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
       $this->name = $name;
    }

      /**
     * @ORM\Column(type="text" , length=100)
     */
    private $password;

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
       $this->password = $password;
    }

     /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="Groups", inversedBy="users")
     * @ORM\JoinTable(name="user_group")
     */
    private $groups;

    public function __construct() {
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addGroup(Groups $group)
    {
        $group->addGroup($this); // synchronously updating inverse side
        $this->groups[] = $group;
    }

    public function getGroups()
    {
        return $this->groups;
    }
   
}
