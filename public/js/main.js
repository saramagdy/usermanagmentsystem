
const users = document.getElementById('users');

if (users) {
    users.addEventListener('click', e => {
    if (e.target.className === 'btn btn-danger delete-user') {
      if (confirm('Are you sure?')) {
        const id_user = e.target.getAttribute('data-id');

        fetch(`user/delete/${id_user}`, {
          method: 'DELETE'
        }).then(res => window.location.reload()).catch(function (err) { 
          Console.log(err); 
      });
      }
    }
  });
}


const groups = document.getElementById('groups');

if (groups) {
  
    groups.addEventListener('click', e => {
    if (e.target.className === 'btn btn-danger delete-group') {
     
      if (confirm('Are you sure?')) {
        const id = e.target.getAttribute('data-id');
       
        fetch(`group/delete/${id}`, {
          method: 'DELETE'
        }).then(res => window.location.reload()).catch(function (err) { 
          Console.log(err); 
      });
      }
    }
  });
}